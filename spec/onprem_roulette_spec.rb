require 'rspec'
require 'onprem_roulette'

RSpec.describe OnPeep do
  describe '#initialize' do
    it 'creates OnPrem employees with the correct information' do
      onpeep = OnPeep.new('Taylor', 'Austin', 'Hasbro')
      expect(onpeep.name).to eq('Taylor')
      expect(onpeep.location).to eq('Austin')
      expect(onpeep.project).to eq('Hasbro')
    end
  end
end


RSpec.describe OnPremRoulette do
  before(:each) do
    @onprem_roulette = OnPremRoulette.new
    @roster = [["Name", "Location", "Project"],
               ["Taylor Smith", "Austin", "Hasbro"],
               ["Bob Reed", "Austin", "Hasbro"],
               ["Pete Leone", "Los Angeles", "Disney"],
               ["Grant Gustafson", "Los Angeles", "-"],
               ["Jessica Chavez", "Austin", "-"]]
  end

  describe '#initialize' do
    it 'creates an instance of OnPremRoulette with two variables' do
      expect(@onprem_roulette.matches).to eq({})
    end
  end

  describe '#get_spreadsheet' do
    it 'should read in the csv' do
      CSV.should_receive(:read).with('roster.csv')
      @onprem_roulette.get_spreadsheet
    end
  end

  describe '#remove_top_row' do
    it 'removes the top row from the imported csv data' do
      roster = @onprem_roulette.remove_top_row(@roster)
      expect(roster).to eq([["Taylor Smith", "Austin", "Hasbro"],
                           ["Bob Reed", "Austin", "Hasbro"],
                           ["Pete Leone", "Los Angeles", "Disney"],
                           ["Grant Gustafson", "Los Angeles", "-"],
                           ["Jessica Chavez", "Austin", "-"]])
    end
  end
end