OnPrem Roulette is a program that takes in a company roster in the form of a CSV file and randomly pairs employees while taking into consideration factors such as location and project.

If you would like to put your own CSV into OnPrem Roulette, please follow the example in roster.csv. If the rows do not match up exactly (Name, Location, Project), the program will not work.