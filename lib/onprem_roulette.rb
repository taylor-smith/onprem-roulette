require 'csv'
require 'pry'

class OnPeep
  attr_reader :name, :location, :project
  def initialize(name, location, project)
    @name = name
    @location = location
    @project = project
  end
end


class OnPremRoulette
  
  attr_accessor :first_matches, :second_matches, :location_map, :project_map
  def initialize
    @first_matches = Hash.new
    @second_matches = Hash.new
    @location_map = Hash.new
    @project_map = Hash.new
  end

  # Public: Begin the OnPrem Roulette program by grabbing data from a properly
  # formatted csv.

  # Examples
  # roulette = OnPremRoulette.new
  # roster = roulette.get_spreadsheet
  # # => [["Taylor Smith", "Austin", "Hasbro"],
  #       ["Bob Reed", "Austin", "Hasbro"],
  #        ... ]
  public
  def get_spreadsheet
    roster = CSV.read("roster.csv")
    # Remove the column names from the data
    roster.shift
    return roster
  end

  # Public: Randomly match employees up with one another, creating an OnPeep
  # object for each row in the CSV file. These OnPeep objects will be used
  # to randomly assign pairs.
  def create_onpeeps(roster)
    onpeeps = Array.new
    roster.each do |entry|
      onpeep = OnPeep.new(entry[0], entry[1], entry[2])      
      onpeeps.push(onpeep)
    end
    build_location_buckets(onpeeps)
    build_project_buckets(onpeeps)
    assign_first_round_partners(onpeeps)
  end

  def build_location_buckets(roster)
    roster.each do |employee|
      if @location_map[employee.location] == nil
        @location_map[employee.location] = {}
        @location_map[employee.location][employee.name] = employee
      else
        @location_map[employee.location][employee.name] = employee
      end
    end
  end

  def build_project_buckets(roster)
    roster.each do |employee|
      if @project_map[employee.project] == nil
        @project_map[employee.project] = {}
        @project_map[employee.project][employee.name] = employee
      else
        @project_map[employee.project][employee.name] = employee
      end
    end
  end

  # Private: Run the loop that will assign matches to employees. If the match
  # passes all the necessary checks, assign it to the @matches instance variable
  # in a two-way lookup (employee => match and match => employee).
  private
  def assign_first_round_partners(roster)
    transient_location_map = deep_copy(@location_map)
    transient_project_map = deep_copy(@project_map)
    roster.each do |employee|
        if !@first_matches[employee]
          if check_for_empty_locations(transient_location_map)
            match = self.get_first_match_with_same_location(roster, employee)
            create_first_match(employee, match, transient_location_map, transient_project_map)
          else
            # Don't pair with others from the same location
            match = self.get_first_match(roster, employee)
            create_first_match(employee, match, transient_location_map, transient_project_map)
          end
        end
    
    end
    assign_second_round_partners(roster)
  end

  def assign_second_round_partners(roster)
    transient_location_map = deep_copy(@location_map)
    transient_project_map = deep_copy(@project_map)
    roster.each do |employee|
      if !@second_matches[employee]
        if check_for_empty_locations(transient_location_map)
          match = self.get_second_match_with_same_location(roster, employee)
          create_second_match(employee, match, transient_location_map, transient_project_map)
        else
          # Don't pair with others from the same location
          match = self.get_second_match(roster, employee)
          create_second_match(employee, match, transient_location_map, transient_project_map)
        end
      end
    end
    write_csv
  end

  def create_first_match(employee, match, transient_location_map, transient_project_map)
    puts "found a match"
    @first_matches[employee] = match
    @first_matches[match] = employee
    remove_employee_and_match_from_location_and_project_map(employee, match, transient_location_map, transient_project_map)
  end

  def create_second_match(employee, match, transient_location_map, transient_project_map)
    puts "found a match"
    @second_matches[employee] = match
    @second_matches[match] = employee
    remove_employee_and_match_from_location_and_project_map(employee, match, transient_location_map, transient_project_map)
  end

  def remove_employee_and_match_from_location_and_project_map(employee, match, transient_location_map, transient_project_map)
    transient_location_map[employee.location].delete(employee.name)
    transient_project_map[employee.project].delete(employee.name)
    transient_location_map[match.location].delete(match.name)
    transient_project_map[match.project].delete(match.name)
  end

  def check_for_empty_locations(location_map)
    if location_map["Austin"] == {} || location_map["Los Angeles"] == {}
      return true
    else
      return false
    end
  end

  protected
  def get_first_match(roster, employee)
    match = roster.sample
    if @first_matches[match] || !check_duplicate(employee, match) || !check_diff_location(employee, match) || !check_diff_project(employee, match)
      get_first_match(roster, employee)
    else
      return match
    end
  end

  def get_first_match_with_same_location(roster, employee)
    match = roster.sample
    if @first_matches[match] || !check_duplicate(employee, match) || !check_diff_project(employee, match)
      get_first_match_with_same_location(roster, employee)
    else
      return match
    end
  end

  def get_second_match(roster, employee)
    match = roster.sample
    if @second_matches[match] || @first_matches[employee] == match || !check_duplicate(employee, match) || !check_diff_location(employee, match) || !check_diff_project(employee, match)
      get_second_match(roster, employee)
    else
      return match
    end
  end

  def get_second_match_with_same_location(roster, employee)
    match = roster.sample
    if @second_matches[match] || @first_matches[employee] == match || !check_duplicate(employee, match) || !check_diff_project(employee, match)
      get_second_match_with_same_location(roster, employee)
    else
      return match
    end
  end

  private
  def check_duplicate(employee, match)
    if employee == match
      puts "found a duplicate"
      return false
    else
      return true
    end
  end

  def check_diff_location(employee, match)
    if employee.location == match.location
      return false
    else
      return true
    end
  end

  def check_diff_project(employee, match)
    if employee.project == match.project && ((employee.project || match.project) != "-")
      puts "found a match from the same project"
      return false
    else
      return true
    end
  end

  def write_csv
    CSV.open("ouput" + DateTime.now.to_s + ".csv", "wb") do |csv|
      csv << ["Employee", "Match"]
      @first_matches.each_with_index do |match, index|
        if index % 2 === 0
          csv << [match[0].name, match[1].name]
        end
      end
      csv << ["Employee2", "Match2"]
      @second_matches.each_with_index do |match, index|
        if index % 2 === 0
          csv << [match[0].name, match[1].name]
        end
      end
    end
  end

  # Helper method to return a deep copy of a hash
  def deep_copy(o)
    Marshal.load(Marshal.dump(o))
  end
end

roulette = OnPremRoulette.new
roster = roulette.get_spreadsheet
roulette.create_onpeeps(roster)