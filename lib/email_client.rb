require 'google/api_client'
require 'google/api_client/client_secrets'
require 'google/api_client/auth/installed_app'
require 'google/api_client/auth/storage'
require 'google/api_client/auth/storages/file_store'
require 'fileutils'

class OnPremRouletteEmailClient
  APPLICATION_NAME = 'Google Calendar API Quickstart'
  CLIENT_SECRETS_PATH = 'client_secret.json'
  CREDENTIALS_PATH = File.join(Dir.home, '.credentials',
                               "onprem-roulette.json")
  SCOPE = 'https://www.googleapis.com/auth/calendar'

  ##
  # Ensure valid credentials, either by restoring from the saved credentials
  # files or intitiating an OAuth2 authorization request via InstalledAppFlow.
  # If authorization is required, the user's default browser will be launched
  # to approve the request.
  #
  # @return [Signet::OAuth2::Client] OAuth2 credentials
  def authorize
    FileUtils.mkdir_p(File.dirname(CREDENTIALS_PATH))

    file_store = Google::APIClient::FileStore.new(CREDENTIALS_PATH)
    storage = Google::APIClient::Storage.new(file_store)
    auth = storage.authorize

    if auth.nil? || (auth.expired? && auth.refresh_token.nil?)
      app_info = Google::APIClient::ClientSecrets.load(CLIENT_SECRETS_PATH)
      flow = Google::APIClient::InstalledAppFlow.new({
        :client_id => app_info.client_id,
        :client_secret => app_info.client_secret,
        :scope => SCOPE})
      auth = flow.authorize(storage)
      puts "Credentials saved to #{CREDENTIALS_PATH}" unless auth.nil?
    end
    auth
  end

  # Initialize the API
  def start
    client = Google::APIClient.new(:application_name => APPLICATION_NAME)
    client.authorization = authorize
    calendar_api = client.discovered_api('calendar', 'v3')

    event = {
      'summary' => 'TaylorTestCalendarInvite',
      'location' => 'OnPrem Roulette',
      'description' => 'A chance to hear more about Google\'s developer products.',
      'start' => {
        'dateTime' => '2015-08-24T09:00:00-07:00',
        'timeZone' => 'US/Central',
      },
      'end' => {
        'dateTime' => '2015-08-24T17:00:00-07:00',
        'timeZone' => 'US/Central',
      },
      'attendees' => [
        # {'email' => 'lpage@example.com'},
        # {'email' => 'sbrin@example.com'},
      ],
      'reminders' => {
        'useDefault' => true,
      }
    }

    results = client.execute!(
      :api_method => calendar_api.events.insert,
      :parameters => {
        :calendarId => 'primary'},
      :body_object => event)
    event = results.data
    puts "Event created: #{event.htmlLink}"
  end
end

email_client = OnPremRouletteEmailClient.new
email_client.start